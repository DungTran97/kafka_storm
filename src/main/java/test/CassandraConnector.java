package test;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.InvalidQueryException;
import com.datastax.driver.core.Cluster.Builder;

public class CassandraConnector {
	private Cluster cluster;
	private Session session;
	
	public void connect(String node, Integer port){
	    Builder b = Cluster.builder().addContactPoint(node);
        if (port != null) {
            b.withPort(port);
        }
        this.cluster = b.build();
 
        this.session = cluster.connect();
    }
	
    public Session getSession() {
        return this.session;
    }

    public void close() {
        session.close();
        cluster.close();
    }
    
    public void insertData(String data) {
    	try {
    	this.session.execute(String.format("INSERT INTO test.new_test(id, content) VALUES (uuid(), '%s');", data));
    	System.out.println("Inserted");
    	} catch( InvalidQueryException err) {
    		
    	}
    }
}
