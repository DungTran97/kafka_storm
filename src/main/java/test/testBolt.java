package test;

import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;

import test.CassandraConnector;

public class testBolt implements IRichBolt {
	private OutputCollector collector;
	private CassandraConnector conn;
	@Override
	public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
		conn = new CassandraConnector();
		conn.connect("209.97.173.119", 2810);
	}
	
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		System.out.println("Message: " + input.getString(4));
		conn.insertData(input.getString(4));
	}

	public void cleanup() {
		// TODO Auto-generated method stub
		conn.close();
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		
	}

	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}
}
