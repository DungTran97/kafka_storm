package test;

import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.kafka.spout.FirstPollOffsetStrategy;
import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.kafka.spout.KafkaSpoutConfig;

public class mainTopology {
	public static void main(String[] args) throws InterruptedException, AlreadyAliveException, InvalidTopologyException, AuthorizationException{
		//Build Topology	
		TopologyBuilder builder = new TopologyBuilder();
		
		builder.setSpout("Kafka-Spout", new KafkaSpout<>(KafkaSpoutConfig.builder("localhost:9092", "kafka-storm")
																		.setFirstPollOffsetStrategy(FirstPollOffsetStrategy.UNCOMMITTED_LATEST)
																		.build()));
		
		builder.setBolt("Kafka-Bolt", new testBolt())
				.shuffleGrouping("Kafka-Spout");
		
		StormTopology topology = builder.createTopology();
		
		//Configuration
		Config conf = new Config();
		conf.setDebug(false);
		conf.setNumWorkers(4);
		StormSubmitter.submitTopology("test_topology", conf, topology);
	}
}
